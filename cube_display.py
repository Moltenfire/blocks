from opengl_display import OpenGLDisplay, Cube, Point3
from itertools import product

colours = (
    (1,0,0),
    (0,1,0),
    (0,0,1),
    (1,1,0),
    (1,0,1),
    (0,1,1),
    (0.5,1,0),
    (1,0.5,0),
    (0.5,0,1),
    (1,0,0.5),
    (0,0.5,1),
    (0,1,0.5),
    (1,1,1),
)

class CubeDisplay(OpenGLDisplay):

    def __init__(self, size, axis=False):
        self.size = size
        self.cubes = []

    def _translate(self, point):
        half_size = self.size / 2
        return Point3(point.x - half_size, point.y - half_size, point.z - half_size)

    def add_cube(self, x, y, z, colour):
        p0 = Point3(x,y,z)
        # print(p0)
        p = self._translate(p0)
        # print(p)
        cube = Cube.from_corner(p, 1, colour)
        self.cubes.append(cube)

    def clear_cubes(self):
        self.cubes.clear()

    def draw(self):
        for cube in self.cubes:
            cube.draw_faces()
        for cube in self.cubes:
            cube.draw_lines()

def main():
    cube_display = CubeDisplay(4)
    for x, y, z in product(range(4), repeat=3):
        cube_display.add_cube(Point3(x,y,z), colours[z])
    cube_display.run()

if __name__=='__main__':
    main()
