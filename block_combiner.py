from block_definitions import get_all_positions_to_block_id
from collections import deque
from time import time

def get_lowest_unfilled(grid):
    for i in range(64):
        if not (1 << i & grid):
            return i

corner_indexes = [0, 3, 12, 15, 48, 51, 60, 63]
def get_unfilled(grid):
    for i in corner_indexes:
        if not (1 << i & grid):
            return i
    return get_lowest_unfilled(grid)

def get_possible_blocks_matching_unfilled(all_blocks, chosen_block_ids, grid, unfilled):
    unfilled_pattern = 1 << unfilled
    for block, block_id in all_blocks.items():
        if unfilled_pattern & block and block_id not in chosen_block_ids:
            if not (grid & block):
                yield block

def get_possible_blocks(all_blocks, chosen_block_ids, grid):
    unfilled = get_unfilled(grid)
    return get_possible_blocks_matching_unfilled(all_blocks, chosen_block_ids, grid, unfilled)

def fill_a_block(all_blocks, grid, chosen_blocks, chosen_block_ids):
    possible_blocks = get_possible_blocks(all_blocks, chosen_block_ids, grid)

    res = []
    for chosen_block in possible_blocks:
        chosen_block_id = all_blocks[chosen_block]
        new_grid = grid | chosen_block
        new_chosen_blocks = chosen_blocks[:]
        new_chosen_block_ids = chosen_block_ids.copy()
        new_chosen_blocks.append(chosen_block)
        new_chosen_block_ids.add(chosen_block_id)

        res.append((new_grid, new_chosen_blocks, new_chosen_block_ids))

    return res

full_grid = 2**64 - 1

def combine_blocks(all_blocks, grid=0, chosen_blocks=[], chosen_block_ids=set(), filename="found.txt"):

    queue = deque([(grid, chosen_blocks, chosen_block_ids)])

    with open(filename, 'w'):
        pass

    start_time = time()
    previous_current_time = start_time

    i = 0
    total_found = 0
    while queue:
        grid, chosen_blocks, chosen_block_ids = queue.popleft()

        if grid == full_grid:
        # if len(chosen_blocks) == 13:
            # print(i, "Found combo")
            print(chosen_blocks)
            # print(len(chosen_blocks))
            total_found += 1

            with open(filename, 'a') as f:
                f.write(str(chosen_blocks))
                f.write('\n')

            continue

        possible_next_moves = fill_a_block(all_blocks, grid, chosen_blocks, chosen_block_ids)

        if possible_next_moves:
            queue.extendleft(reversed(possible_next_moves))

        i += 1
        n = 10000
        if i % n == 0:
            current_time = time()
            total_time = current_time - start_time
            elapsed_time = current_time - previous_current_time
            previous_current_time = current_time
            print(i, "Total Found:", total_found, "Queue:", len(queue), "Total time {:.3f}s Iters/s: {:.3f}".format(total_time, n / elapsed_time))

        # if i >= 20000:
        #     break

    current_time = time()
    total_time = current_time - start_time
    iters_per_second = i / total_time if total_time > 0 else 0
    print("Done", "iterations:", i, "Total found:", total_found)
    print("Total time {:.3f}s Iters/s: {:.3f}".format(total_time, iters_per_second))

    return total_found, total_time, i

if __name__=='__main__':
    all_blocks = get_all_positions_to_block_id()
    combine_blocks(all_blocks)
