import pytest
from block_definitions import get_all_positions_to_block_id, int_to_position, position_to_int


def test_int_to_position():
    all_blocks = get_all_positions_to_block_id()

    for b0 in all_blocks:
        position = int_to_position(b0, 4)
        b1 = position_to_int(position)
        print(bin(b0))
        print(bin(b1))
        assert(b0 == b1)
