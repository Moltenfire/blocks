from combiner_block_display import CombinerBlockDisplay
from block_display import MultiBlockDisplay
from block_definitions import int_to_position

l = [65559, 6291656, 10016, 549965531136, 838864896, 149535728910336, 4503681231880192, 4690105073664, 1152940196322344960, 13835163609472172032, 632755782005293056, 10977661530669056, 2810248366502445056]
l = [1585, 134219916, 536883456, 1174454272, 281479274889216, 3378283836604416, 1224980266892656640, 9223525970630148096, 163208888322, 274890752064, 2305897985063518208, 5512476312645664768, 176203335420870656]
# for x in l:
#     print(bin(x)[2:].zfill(64))



size = 4

# for i in l:
#     score = int_to_placement_score(i, size)
#     print(i, score)

cbd = CombinerBlockDisplay(4, l)
cbd.run()

# mbd = MultiBlockDisplay(4, [int_to_position(i, 4) for i in sorted(reversed(l))])
# mbd.run()
