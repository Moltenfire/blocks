from block_definitions import get_all_positions_to_block_id, int_to_position, position_to_int, get_all_rotations
from block_combiner import get_possible_blocks, combine_blocks
import os
from collections import Counter
# from block_display import MultiBlockDisplay

def get_start_block_rotations():
    all_blocks = get_all_positions_to_block_id()
    all_start_blocks = get_possible_blocks(all_blocks, set(), 0)

    remaining_start_blocks = set(all_start_blocks)

    start_block_rotations = {}
    while remaining_start_blocks:
        block_id = sorted(remaining_start_blocks)[0]
        block = int_to_position(block_id, 4)
        rotations = set(map(position_to_int, get_all_rotations(block)))

        overlap = remaining_start_blocks & rotations

        c = sorted(overlap)[0]
        start_block_rotations[c] = rotations
        remaining_start_blocks -= overlap

    
    return start_block_rotations

def multiply_all(g):
    l = list(g)
    res = l[0]
    for i in l[1:]:
        res *= i
    return res


def run(all_blocks, start_block_rotations, start_blocks, i):

    start_block = start_blocks[i]
    block_num = all_blocks[start_block]

    # print(block_num, start_block)

    previous_start_blocks = start_blocks[:i]
    all_previous_start_blocks = set()
    for block in previous_start_blocks:
        all_previous_start_blocks.update(start_block_rotations[block])


    # reduced_blocks = {b : num for b, num in all_blocks.items() if b not in all_previous_start_blocks} # num != block_num and 
    # reduced_blocks = {b : num for b, num in all_blocks.items() if num != block_num }
    reduced_blocks = {b : num for b, num in all_blocks.items() if b not in all_previous_start_blocks and num != block_num }

    # print(len(previous_start_blocks), len(all_blocks))
    counts = dict(Counter(reduced_blocks.values()))
    x = multiply_all(counts.values())
    all_x = multiply_all(dict(Counter(all_blocks.values())).values())
    # print(dict(Counter(all_blocks.values())))
    # print(str(i).zfill(2), block_num, counts, all_x / x)
    print(str(i).zfill(2), block_num, x, all_x / x)


all_blocks = get_all_positions_to_block_id()
start_block_rotations = get_start_block_rotations()
start_blocks = sorted(start_block_rotations.keys(), key=lambda x : all_blocks[x])

for i, block in enumerate(all_blocks):
    print(i, block, all_blocks[block])

# for i in range(28):
#     run(all_blocks, start_block_rotations, start_blocks, i)
