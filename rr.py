from block_definitions import get_all_positions_to_block_id, int_to_position
# from block_display import BlockDisplay
from block_combiner import get_possible_blocks_matching_unfilled
import numpy as np
from itertools import permutations, combinations
from collections import Counter

all_blocks = get_all_positions_to_block_id()
possible_blocks = list(get_possible_blocks_matching_unfilled(all_blocks, set(), 0, 12))
# block_ids = [all_blocks[block] for block in possible_blocks]
# counts = Counter(block_ids)

# print(counts.values())

for i in range(64):
    possible_blocks = list(get_possible_blocks_matching_unfilled(all_blocks, set(), 0, i))
    print(i, len(possible_blocks))

# # perms = list(permutations(range(12), 4))
# perms = list(permutations(list(counts.values()), 4))

# total = 0
# for a, b, c, d in perms:
#     t = a * b * c * d
#     total += t

# print(total)
# print(total / len(perms))

