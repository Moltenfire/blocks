import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from collections import namedtuple

Point3 = namedtuple('Point3', 'x, y, z')

class OpenGLDisplay():

    def run(self):
        pygame.init()
        # display = (600,450)
        display = (800,600)
        pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

        # glClearColor(0.0, 0.0, 0.0, 0.0)
        # glClearDepth(1.0)

        glDepthMask(GL_TRUE)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LESS)
        glEnable(GL_CULL_FACE)
        glCullFace(GL_BACK)
        glFrontFace(GL_CCW)
        glShadeModel(GL_SMOOTH)
        # glDepthRange(0.0,1.0)

        glLineWidth(4)

        gluPerspective(45, (display[0]/display[1]), 0.1, 50.0)

        glTranslatef(0,0, -10)

        glRotatef(30, 1, 0, 0)
        glRotatef(45, 0, 1, 0)

                # glRotatef(90, 0, 1, 0)
        # glRotatef(-45, 0, 0, 1)
        # glRotatef(45, 1, 0, 0)

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self.left_key()
                    if event.key == pygame.K_RIGHT:
                        self.right_key()
                    if event.key == pygame.K_DOWN:
                        self.down_key()
                    if event.key == pygame.K_UP:
                        self.up_key()
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        quit()

            glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
            self.draw()
            pygame.display.flip()
            pygame.time.wait(10)

    def left_key(self):
        glRotate(-30, 0, 1, 0)

    def right_key(self):
        glRotate(30, 0, 1, 0)

    def down_key(self):
        pass

    def up_key(self):
        pass

edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,3),
    (2,7),
    (6,3),
    (6,4),
    (6,7),
    (5,1),
    (5,4),
    (5,7)
    )

surfaces = (
    (0,1,2,3),
    (3,2,7,6),
    (6,7,5,4),
    (4,5,1,0),
    (1,5,7,2),
    (4,0,3,6)
    )

class Cube():

    def __init__(self, x, z, y, size, colour):
        s = size / 2
        self.vertices = (
            (x+s, y-s, z-s),
            (x+s, y+s, z-s),
            (x-s, y+s, z-s),
            (x-s, y-s, z-s),
            (x+s, y-s, z+s),
            (x+s, y+s, z+s),
            (x-s, y-s, z+s),
            (x-s, y+s, z+s)
        )
        self.colour = colour

    @classmethod
    def from_corner(cls, point, size, colour):
        half_size = size / 2
        p = Point3(point.x + half_size, point.y + half_size, point.z + half_size)
        return cls(p.x, p.y, p.z, size, colour)

    def draw_faces(self):
        glBegin(GL_QUADS)
        for surface in surfaces:
            for vertex in surface:
                glColor3fv(self.colour)
                glVertex3fv(self.vertices[vertex])
        glEnd()

    def draw_lines(self):
        glBegin(GL_LINES)
        for edge in edges:
            for vertex in edge:
                glColor3fv((0, 0, 0))
                glVertex3fv(self.vertices[vertex])
        glEnd()