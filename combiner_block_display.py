from cube_display import CubeDisplay, colours
from block_definitions import int_to_position
import numpy as np

class CombinerBlockDisplay(CubeDisplay):

    def __init__(self, size, block_ints):
        super(CombinerBlockDisplay, self).__init__(size, True)

        self.block_ints = block_ints
        self.blocks = [int_to_position(i, self.size) for i in self.block_ints]
        self.num_blocks = len(self.blocks)
        self.index = self.num_blocks
        self.update()


    def down_key(self):
        index = max(0, self.index - 1)
        if index != self.index:
            self.index = index
            self.update()

    def up_key(self):
        index = min(self.num_blocks, self.index + 1)
        if index != self.index:
            self.index = index
            self.update()

    def update(self):
        self.clear_cubes()

        grid = 0

        for i in range(self.index):
            block = self.blocks[i]
            colour = colours[i]
            self.add_cubes_from_block(block, colour)

            overlap = grid & self.block_ints[i]

            one_count = np.sum(block)

            grid |= self.block_ints[i]
            print(bin(self.block_ints[i])[2:].zfill(64), self.block_ints[i], overlap, one_count)


        print(bin(grid)[2:].zfill(64))
        print()
        # if self.index:
            # i = self.index - 1


    def add_cubes_from_block(self, block, colour):
        for x in range(block.shape[0]):
            for y in range(block.shape[1]):
                for z in range(block.shape[2]):
                    if block[x,y,z]:
                        # print(x,y,z)
                        self.add_cube(x, y, z, colour)
