import numpy as np
# from cube_display import CubeDisplay
from block_definitions import get_blocks
from block_display import BlockDisplay, MultiBlockDisplay

arr0 = np.array([0,1,0,1,1,1,0,1,0]).reshape((3,3,1))
# arr0 = np.array([0,0,1,0,0,0,1,0,1,1,1,0,0,0,1,0,0,0]).reshape((3,3,2))
# arr0 = np.rot90(arr0, axes=(0,2))

arr1 = np.array([1,0,0,0,1,0,1,1,1,0,0,0]).reshape((3,2,2))
arr1 = np.rot90(arr1, k=2, axes=(0,1))

arr2 = np.array([1,1,1,0,1,0,0,0,1,0,0,0]).reshape((3,2,2))
# arr2 = np.rot90(arr2, k=0, axes=(1,2))
arr2 = np.rot90(arr2, k=2, axes=(0,1))



arr3 = np.array([1,0,0,0,1,1,1,0,1,0,0,0]).reshape((3,2,2))
arr3 = np.rot90(arr3, k=2, axes=(1,2))

arr4 = np.array([1,0,0,1,1,0,0,1,1]).reshape((3,3,1))
arr5 = np.array([0,1,0,0,1,1,1,1,0]).reshape((3,3,1))

arr6 = np.array([1,0,0,0,1,0,1,0,1,1,0,0]).reshape((3,2,2))

arr7 = np.array([0,0,1,0,1,1,1,0,0,1,0,0]).reshape((3,2,2))

arr8 = np.array([0,0,1,0,1,0,1,0,1,1,0,0]).reshape((3,2,2))
arr9 = np.array([0,0,1,0,1,1,1,0,1,0,0,0]).reshape((3,2,2))

arr10 = np.array([1,0,1,0,1,0,0,0,1,1,0,0]).reshape((3,2,2))
arr11 = np.array([1,0,0,0,1,0,0,0,1,0,1,1]).reshape((3,2,2))
arr12 = np.array([1,0,0,0,1,1,0,1]).reshape((2,2,2))

arr = arr12
# arr = np.rot90(arr, k=2, axes=(0,1))
print(','.join(map(str, arr.flatten())))

bd = BlockDisplay(3, arr)
# bd.run()

mbd = MultiBlockDisplay(3, get_blocks())
mbd.run()

# def add_blocks(cd, arr, translate=(0,0,0), colour=(1,0,0)):
#     for x in range(arr.shape[0]):
#         for y in range(arr.shape[1]):
#             for z in range(arr.shape[2]):
#                 if arr[x,y,z]:
#                     cd.add_cube(Point3(x+translate[0],y+translate[1],z+translate[2]), colour)

# cd = CubeDisplay(3)
# add_blocks(cd, arr0)
# add_blocks(cd, arr1, colour=(0,1,0))
# add_blocks(cd, arr2, translate=(0,1,0), colour=(0,0,1))
# add_blocks(cd, arr3, translate=(0,1,1), colour=(1,1,0))
# cd.run()

# print(arr0)



# size = 3

# unique_positions = get_unique_positions(arr0, size)

# print(unique_positions)
# print(len(get_unique_ints(unique_positions)))

# mbd = MultiBlockDisplay(3, positions)
# mbd = MultiBlockDisplay(3, arrays)
# mbd.run()

# for p in positions:
    # print(p)

# print(sorted(list(set(positions))))
