from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt

fig = plt.figure()
ax = Axes3D(fig)
verts = []
x = [0, 0.95, 0.95, 0]
y = [0, 0, 0.95, 0.95]
z0 = [0, 0, 0, 0]
z1 = [0.95, 0.95, 0.95, 0.95]
bottom_face = list(zip(x, y, z0))
top_face = list(zip(x, y, z1))

corner_lines = list(zip(bottom_face, top_face))
for i in range(4):
    p0, p1 = corner_lines[i]
    p3, p2 = corner_lines[(i+1)%4]

    side_face = [p0, p1, p2, p3]
    verts.append(side_face)

verts.append(top_face)
verts.append(bottom_face)
# ax.plot([0, 1, 1, 0], [0, 0, 0, 0], [0, 0, 1, 1], color='black', linewidth=5)
# ax.add_collection3d(Poly3DCollection(verts), zs='z')

ax.plot_surface(x, y, z0)


plt.show()

