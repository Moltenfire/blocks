from block_definitions import get_all_positions_to_block_id, int_to_position, position_to_int, get_all_rotations
from block_combiner import get_possible_blocks, combine_blocks
import os
# from block_display import MultiBlockDisplay

def get_start_block_rotations():
    all_blocks = get_all_positions_to_block_id()
    all_start_blocks = get_possible_blocks(all_blocks, set(), 0)

    remaining_start_blocks = set(all_start_blocks)

    start_block_rotations = {}
    while remaining_start_blocks:
        block_id = sorted(remaining_start_blocks)[0]
        block = int_to_position(block_id, 4)
        rotations = set(map(position_to_int, get_all_rotations(block)))

        overlap = remaining_start_blocks & rotations

        start_block_rotations[sorted(overlap)[0]] = rotations
        remaining_start_blocks -= overlap

    
    return start_block_rotations


def run(i):

    all_blocks = get_all_positions_to_block_id()
    start_block_rotations = get_start_block_rotations()
    start_blocks = sorted(start_block_rotations.keys(), key=lambda x : all_blocks[x])

    start_block = start_blocks[i]

    previous_start_blocks = start_blocks[:i]
    all_previous_start_blocks = set()
    for block in previous_start_blocks:
        all_previous_start_blocks.update(start_block_rotations[block])


    block_num = all_blocks[start_block]
    all_blocks = {b : num for b, num in all_blocks.items() if num != block_num and b not in all_previous_start_blocks } 

    print(start_block, block_num, len(previous_start_blocks), len(all_previous_start_blocks), len(all_blocks))

    filename_tmp = "patterns/_patterns_{}.txt".format(i)
    filename = "patterns/patterns_{}.txt".format(i)
    total_found, total_time, iterations = combine_blocks(all_blocks, start_block, [start_block], set([start_block]), filename_tmp)

    if os.path.exists(filename):
        os.remove(filename)
    os.rename(filename_tmp, filename)

    with open("patterns/data.csv", 'a') as f:
        f.write(','.join(map(str, [i, total_found, total_time, iterations])))
        f.write('\n')

# for i in reversed(range(12, 18)):
for i in reversed(range(10, 15)):
    run(i)
    print()

# run(27)
