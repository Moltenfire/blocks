from cube_display import CubeDisplay
from opengl_colours import Red
import numpy as np
from block_definitions import int_to_position

class BlockDisplay(CubeDisplay):

    def __init__(self, size, arr):
        super(BlockDisplay, self).__init__(size)
        self.arr = arr

        self.create_block()

    def create_block(self):
        self.clear_cubes()
        for x in range(self.arr.shape[0]):
            for y in range(self.arr.shape[1]):
                for z in range(self.arr.shape[2]):
                    if self.arr[x,y,z]:
                        
                        self.add_cube(x, y, z, Red)

    def left_key(self):
        self.arr = np.rot90(self.arr, k=1, axes=(0,1))
        self.create_block()

    def right_key(self):
        self.arr = np.rot90(self.arr, k=3, axes=(0,1))
        self.create_block()

    def down_key(self):
        self.arr = np.rot90(self.arr, k=1, axes=(0,2))
        self.create_block()

    def up_key(self):
        self.arr = np.rot90(self.arr, k=3, axes=(0,2))
        self.create_block()

class MultiBlockDisplay(BlockDisplay):

    def __init__(self, size, arrays):
        super(MultiBlockDisplay, self).__init__(size, arrays[0])
        self.index = 0
        self.arrays = arrays

    def left_key(self):
        self.index = (self.index - 1) % len(self.arrays)
        self.arr = self.arrays[self.index]
        # print(self.index)
        self.create_block()
    
    def right_key(self):
        self.index = (self.index + 1) % len(self.arrays)
        # print(self.index)
        self.arr = self.arrays[self.index]
        self.create_block()

# arr = np.array([1,0,0,0,1,1,1,0,1,0,0,0]).reshape((3,2,2))
# arr = np.rot90(arr, k=2, axes=(1,2))

# arr = int_to_position(209715336, 4)

# bd = BlockDisplay(4, arr)
# bd.run()

