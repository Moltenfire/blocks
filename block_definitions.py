import numpy as np
from pprint import pprint
from collections import defaultdict

def get_blocks():

    blocks = []

    # Red
    blocks.append(np.array([0,1,0,1,1,1,0,1,0]).reshape((3,3,1))) # Cross
    blocks.append(np.array([1,1,1,0,1,0,0,0,1,0,0,0]).reshape((3,2,2)))

    blocks.append(np.array([0,0,1,0,1,0,1,0,1,1,0,0]).reshape((3,2,2)))
    blocks.append(np.array([0,0,1,0,1,1,1,0,1,0,0,0]).reshape((3,2,2)))

    # Yellow
    blocks.append(np.array([1,0,0,0,1,1,1,0,1,0,0,0]).reshape((3,2,2)))
    blocks.append(np.array([1,0,0,1,1,0,0,1,1]).reshape((3,3,1))) # Stairs
    blocks.append(np.array([1,0,1,0,1,0,0,0,1,1,0,0]).reshape((3,2,2)))
    blocks.append(np.array([1,0,0,0,1,0,0,0,1,0,1,1]).reshape((3,2,2)))
    blocks.append(np.array([1,0,0,0,1,1,0,1]).reshape((2,2,2)))

    # Blue
    blocks.append(np.array([0,0,1,0,1,1,1,0,0,0,1,0]).reshape((3,2,2)))
    blocks.append(np.array([0,1,0,0,1,1,1,1,0]).reshape((3,3,1)))
    blocks.append(np.array([1,0,0,0,1,0,1,0,1,1,0,0]).reshape((3,2,2)))
    blocks.append(np.array([0,0,1,0,1,1,1,0,0,1,0,0]).reshape((3,2,2)))

    return blocks

def position_to_int(position):
    # f = position.flatten()
    f = (position[x, y, z] for z in range(4) for y in range(4) for x in range(4))

    res = 0
    for i, x in enumerate(f):
        if x:
            res |= 1 << i

    return res

def int_to_position(i, size):
    res = np.zeros((size,size,size), dtype=np.int64)
    for z in range(size):
        for y in range(size):
            for x in range(size):
    # for _ in range(size**3):
                res[x,y,z] = 1 & i
                i = i >> 1
    # return np.array(list(reversed(res))).reshape((size, size, size))
    return res

def get_positions_as_ints(positions):
    return sorted((position_to_int(p) for p in positions))

def get_unique_ints(positions):
    return sorted(list(set((position_to_int(p) for p in positions))))

def get_all_positions(blocks, size):
    positions = []
    for block in blocks:
        mx, my, mz = block.shape

        ix = size - mx + 1 
        iy = size - my + 1 
        iz = size - mz + 1

        for x in range(ix):
            for y in range(iy):
                for z in range(iz):
                    grid = np.zeros((size, size, size), dtype=np.int64)
                    grid[x:x+mx, y:y+my, z:z+mz] = block
                    positions.append(grid)

    return positions

def get_all_rotations(block):
    b0 = block
    b1 = np.rot90(b0, k=1, axes=(0,2))
    b2 = np.rot90(b0, k=2, axes=(0,2))
    b3 = np.rot90(b0, k=3, axes=(0,2))
    b4 = np.rot90(b0, k=1, axes=(0,1))
    b5 = np.rot90(b0, k=3, axes=(0,1))

    rotations = []
    rotations.extend((np.rot90(b0, k=i, axes=(1,2)) for i in range(4)))
    rotations.extend((np.rot90(b1, k=i, axes=(0,1)) for i in range(4)))
    rotations.extend((np.rot90(b2, k=i, axes=(1,2)) for i in range(4)))
    rotations.extend((np.rot90(b3, k=i, axes=(0,1)) for i in range(4)))
    rotations.extend((np.rot90(b4, k=i, axes=(0,2)) for i in range(4)))
    rotations.extend((np.rot90(b5, k=i, axes=(0,2)) for i in range(4)))

    return rotations

def get_unique_positions(block, size):
    all_rotations = get_all_rotations(block)
    all_positions = get_all_positions(all_rotations, size)
    return get_unique_ints(all_positions)

def get_positions_to_block_id(block, block_id, size=4):
    positions_to_block_id = {}
    for unique_position in get_unique_positions(block, size):
        positions_to_block_id[unique_position] = block_id

    return positions_to_block_id

def get_all_positions_to_block_id(size=4):
    positions_to_block_id = {}
    for block_id, block in enumerate(get_blocks()):
        positions_to_block_id.update(get_positions_to_block_id(block, block_id, size))

    return positions_to_block_id

# all_blocks = get_all_positions_to_block_id()

# # print(len(all_blocks))

# for b0 in all_blocks:
#     # print(block)

#     position = int_to_position(b0, 4)

#     b1 = position_to_int(position)

#     assert(b0 == b1)

# b2 = get_blocks()[0]
# positions_to_block_id = get_positions_to_block_id(b2, 2)




# x = sorted(positions_to_block_id.keys())[-1]
# pos = int_to_position(x, 4)
# f_pos = pos.flatten()

# print(bin(x))
# print(pos)
# print(f_pos)
# print()


